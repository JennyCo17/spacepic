const discord = require('discord.js');
const client = new discord.Client();
const request = require('request');
const cron = require('cron');
const fs = require('fs');


client.commands = new discord.Collection();

const commandFiles = fs.readdirSync('./commands/').filter(file => file.endsWith('.js'));
for(let file of commandFiles){
    const command = require(`./commands/${file}`);

    client.commands.set(command.name, command);
}


const token = "YourAwesomeToken";

var prefix = "s!";

client.on('ready', function(){
    console.log("Bot is online");

    const job = new cron.CronJob('0 10 14 * * *', () => {
        let channel = client.channels.get('645617726441914375');
        getImage(channel);
      }, null, true, 'Europe/Paris',null,true);
})

client.on('message', function(msg){
    if(!msg.content.startsWith(prefix)){
        return;
    }
    let args = msg.content.substring(prefix.length).split(" ");
   switch(args[0]){
        case "explain":
            client.commands.get('explain').execute(msg, args);
            break;
        case "image": 
            client.commands.get('image').execute(msg, args);
            break;

        case "help":
            client.commands.get('help').execute(msg, args);
    }
   
})

function getImage(channel){
    

    var options = {
        url: "https://api.nasa.gov/planetary/apod?api_key=QyFS3DKbHIq38PaI5TanzzNgOFZKQwPFhQyRNeef",
        method: "GET"
    }

    request(options, function(error, response, reponseBody){
        if(error){
            console.log(error);
            return;
        }

        var picofday = JSON.parse(reponseBody);

        if(picofday.media_type === "image"){
            let embed = new discord.RichEmbed()
            .setTitle(picofday.title)
            .setImage(picofday.url)
            .setColor(0x270381);
            channel.send(embed);
        }else{
            let splittedLink = picofday.url.split("/")
            let link = "https://www.youtube.com/watch?v="+splittedLink[splittedLink.length-1];
            channel.send(link);
        }
        
    });

}

client.login(token);
