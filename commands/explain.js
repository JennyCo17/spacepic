const request = require('request');
const discord = require('discord.js');
module.exports = {
    name : "explain",
    description : "Retrieve explanation",
    execute(msg, args){
        if(!args[1]){
            var options = {
                url: "https://api.nasa.gov/planetary/apod?api_key=QyFS3DKbHIq38PaI5TanzzNgOFZKQwPFhQyRNeef",
                method: "GET"
            }
        } else if(isValidDate(args[1])){
            var options = {
                url: `https://api.nasa.gov/planetary/apod?api_key=QyFS3DKbHIq38PaI5TanzzNgOFZKQwPFhQyRNeef&date=${args[1]}`,
                method: "GET"
            }
        } else {
            return msg.reply('Please fill your request with a date with the format YYYY-MM-DD');
        }
        
    
        request(options, function(error, response, reponseBody){
            if(error){
                console.log(error);
                return;
            }
    
            var picofday = JSON.parse(reponseBody);

            if(picofday.code){
                return msg.reply(picofday.msg);
            }

            if(picofday.explanation.length>2048){
                let splitted = picofday.explanation.split(".");
                var content = '';
                var totalLength = 0
                for(let index of splitted){
                    console.log(splitted);
                    totalLength+=index.length
                    if(totalLength<=2048){
                        content+=index+'.';
                        totalLength++
                    } else {
                        break;
                    }

                }
            } else {
                var content = picofday.explanation;
            }
    
            let embed = new discord.RichEmbed()
            .setTitle(picofday.title)
            .setDescription(content)
            .setColor(0x270381)
            .setThumbnail(picofday.url);
            msg.channel.send(embed);
        });
    }
}

function isValidDate(dateString) {
    var regEx = /^\d{4}-\d{2}-\d{2}$/;
    if(!dateString.match(regEx)) return false;  // Invalid format
    var d = new Date(dateString);
    var dNum = d.getTime();
    if(!dNum && dNum !== 0) return false; // NaN value, Invalid date
    return d.toISOString().slice(0,10) === dateString;
}