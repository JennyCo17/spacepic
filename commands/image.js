const request = require('request');
const discord = require('discord.js');
module.exports = {
    name : "image",
    description : "Retrieve image",
    execute(msg, args){
        if(!args[1] || !isValidDate(args[1])){
            return msg.reply('Please fill your request with a date with the format YYYY-MM-DD');
        }
        var options = {
            url: `https://api.nasa.gov/planetary/apod?api_key=QyFS3DKbHIq38PaI5TanzzNgOFZKQwPFhQyRNeef&date=${args[1]}`,
            method: "GET"
        }

        request(options, function(error, response, reponseBody){
            if(error){
                console.log(error);
                return;
            }
    
            var picofday = JSON.parse(reponseBody);

            if(picofday.code){
                return msg.reply(picofday.msg);
            }
    
            let embed = new discord.RichEmbed()
            .setTitle(picofday.title)
            .setImage(picofday.url)
            .setColor(0x270381);
            msg.channel.send(embed);
        });
    }
}

function isValidDate(dateString) {
    var regEx = /^\d{4}-\d{2}-\d{2}$/;
    if(!dateString.match(regEx)) return false;  // Invalid format
    var d = new Date(dateString);
    var dNum = d.getTime();
    if(!dNum && dNum !== 0) return false; // NaN value, Invalid date
    return d.toISOString().slice(0,10) === dateString;
}