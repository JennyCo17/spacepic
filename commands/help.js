const discord = require('discord.js');
module.exports = {
    name: "help",
    description: "List of commands and how they work",
    execute(msg, args){
        let embed = new discord.RichEmbed()
        .setTitle("SpacePic Commands")
        .setDescription("SpacePic is a bot made to get the Astronimy Pic of the day. It'll post the picture of the day atomatically."+
        "Its current prefix for its commands is s!. Enjoy the beautiful space on a daily basis.")
        .addField("image", "This command is used to get an image for another day from the API."+
        " Add a date with the YYYY-MM-DD format from Jun 16, 1995 to get a response.")
        .addField("explain", "This command allows you to get the explanation of a picture or video sent by the API."+
        " If nothing is added, you'll get today's explanation. If you fill in a valid date, you'll get the explanation for this date.")
        .setColor(0x270381);

        msg.channel.send(embed);
    }
}